using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlappyBird
{
    public class PipeSpawner : MonoBehaviour
    {
        [SerializeField]
        private PooledObject pipePool;

        [SerializeField]
        private Transform spawnPoint;

        [Space, SerializeField, Range(-1, 1)]
        private float minHeight;
        [SerializeField, Range(-1, 1)]
        private float maxHeight;

        [Space, SerializeField]
        private float timeToSpawnFirstPipe;
        [SerializeField]
        private float timeToSpawnPipe;
        
        [Space, SerializeField]
        private Sprite Day;
        [SerializeField]
        private Sprite Night;
        
        [Space, SerializeField]
        private GameObject pipeDay;
        [SerializeField]
        private GameObject pipeNight;
        
        [SerializeField]
        private SpriteRenderer background;
        

        private void Start()
        {
            RandomizeFunction();
            InitializeObjectPool(pipePool);
            StartCoroutine(SpawnPipes());
        }
        

        private Vector3 GetSpawnPosition()
        {
            return new Vector3(spawnPoint.position.x, Random.Range(minHeight, maxHeight), spawnPoint.position.z);
        }

        private void InitializeObjectPool(PooledObject pool)
        {
            pool.objects = new List<GameObject>();

            for (int i = 0; i < pool.poolSize; i++)
            {
                GameObject obj = Instantiate(pool.prefab, Vector3.zero, Quaternion.identity);
                obj.SetActive(false);
                pool.objects.Add(obj);
            }
        }
        
        private void RandomizeFunction()
        {
            if (Random.Range(0, 2) == 0)
            {
                background.sprite = Day;
                pipePool.prefab = pipeDay;
            }
            else
            {
                background.sprite = Night;
                pipePool.prefab = pipeNight;
            }
        }

        private IEnumerator SpawnPipes()
        {
            yield return new WaitForSeconds(timeToSpawnFirstPipe);

            while (true)
            {
                GameObject pipeObject = GetPooledObject(pipePool);
                pipeObject.transform.position = GetSpawnPosition();
                pipeObject.SetActive(true);

                yield return new WaitForSeconds(timeToSpawnPipe);
            }
        }

        private GameObject GetPooledObject(PooledObject pool)
        {
            foreach (GameObject obj in pool.objects)
            {
                if (!obj.activeInHierarchy)
                {
                    return obj;
                }
            }
            
            GameObject newObj = Instantiate(pool.prefab, Vector3.zero, Quaternion.identity);
            newObj.SetActive(false);
            pool.objects.Add(newObj);

            return newObj;
        }

        public void Stop()
        {
            StopAllCoroutines();
        }
    }
}