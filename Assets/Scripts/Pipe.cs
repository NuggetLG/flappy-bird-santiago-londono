using System.Collections;
using UnityEngine;

namespace FlappyBird
{
    public class Pipe : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float timeToDeactivatePipe;
        
        private void Update()
        {
            if (GameManager.Instance.isGameOver)
                return;

            transform.position += (Vector3.left * Time.deltaTime * speed);
            if (gameObject.activeSelf)
            {
                StartCoroutine(DeactivatePipe());
            }
        }

        private IEnumerator DeactivatePipe()
        {
            yield return new WaitForSeconds(timeToDeactivatePipe);

            gameObject.SetActive(false);
        }
    }
}