using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledObject : MonoBehaviour
{
    public GameObject prefab;
    public int poolSize = 10;
    public List<GameObject> objects;
}
